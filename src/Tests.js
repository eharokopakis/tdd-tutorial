import assert from 'assert';

// Execute these tests via "mocha ./src/Tests.js"

describe('<Module to test>', function() {
    describe('<Function to test>', function() {
        describe('<Variation to test>', function() {
            it('should succeed because it is hard-coded to do so', function() {
                assert.equal(1, 1);
            })
        })
    })
});